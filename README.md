k8s-mysql
==============

It deploys the application mysql on a kubernetes cluster

Requirements
--------------

 - kubernetes

Role Variables
----------------

Default variables:

```yaml
---
## Tasks variables
mysql_manifests_dir: /etc/k8s/manifests/mysql
mysql_namespace: default

## ReplicaController
mysql_container_limits:
  cpu: 200m
  memory: 2Gi

mysql_name: mysql
mysql_replicas: 1


mysql_labels:
  k8s-app: mysql
  version: v1

mysql_selector:
  k8s-app: mysql

# mysql_nodeselector:

## Container 1
### Container 1: Image
mysql_image_registry: Smaine/mydocker
mysql_image_tag: latest

### container 1: Probes
# mysql_container_livenessprobe:
# mysql_container_readinessprobe:

### Container 1: Commands
# mysql_container_command:
#   - /run.sh

# mysql_container_args:
#   - --all

### Container 1: Envs
# mysql_container_env:
#   - name: "DATABASE_URL"
#     value: "postgres://"

### Container 1: Ports
# mysql_container_ports:
#   - containerPort: 3000
#     name: mysql
#     protocol: TCP

### Container 1: Volumes
# mysql_container_volumeMounts:
#   - mountPath: /path_inside_container
#     name: mountpathname

## Host Volumes
# mysql_volumes:
#   - name: mountpathname
#     hostPath:
#       path: /etc/appconf


## Service
# mysql_svc_ports:
#   - name: mysql
#     port: 3000
#     targetPort: 3000
#     protocol: TCP

mysql_svc_type: NodePort

# mysql_svc_clusterIP:
```

Dependencies
------------
 - k8s-common

Example Playbook
----------------
Ferm rules are hash instead of array. The main reason is to be able to merge hashes when configure same host with different roles.
```yaml
- hosts: kube-master
  vars:
    author: ant31
    company: myCompany
    app_name: myapp
    license: MIT
    dest: /tmp/k8s-myapp
    description: K8s role to deploy myapp....
  roles:
    - k8s-myapp
```

License
-------
MIT
